import {tag} from "@prisma/client";
import {ApiProperty} from "@nestjs/swagger";

export class TagDto{
  @ApiProperty()
  postId: number;
  @ApiProperty()
  text: string;
}