import {Injectable} from '@nestjs/common';
import {DatabaseService} from "../database/database.service";
import {TagDto} from "./dto/TagDto";

@Injectable()
export class TagsService {
  constructor(private db: DatabaseService) {
}
  async create(createTagDto: TagDto[]) {
    return createTagDto.map(async (e) => {
      return this.db.tag.create({data: e});
    })
  }

  findAll(id: number) {
    return this.db.tag.findMany({where: {postId: id}})
  }

  findOne(id: number) {
    return this.db.tag.findUnique({where: {id: id}})
  }

  update(id: number, updateTagDto: TagDto) {
    return this.db.tag.update({where:{id: id}, data: updateTagDto})
  }

  remove(id: number) {
    return this.db.tag.delete({where: {id: id}})
  }
}
