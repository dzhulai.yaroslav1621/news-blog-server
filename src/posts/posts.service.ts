import { Injectable } from '@nestjs/common';
import {DatabaseService} from "../database/database.service";
import {Post} from "@prisma/client";
import {PostDto} from "./dto/post.dto";

@Injectable()
export class PostsService {

  constructor(private db: DatabaseService) {
  }
  create(createPostDto: PostDto) {
    return this.db.post.create({data: createPostDto})
  }

  findAll() {
    return this.db.post.findMany()
  }

  findOne(id: number) {
    return this.db.post.findUnique({where: {id: id}})
  }

  update(id: number, updatePostDto: PostDto) {
    return this.db.post.update({where: {id: id}, data: updatePostDto})
  }

  remove(id: number) {
    return this.db.post.delete({where: {id: id}})
  }
}
