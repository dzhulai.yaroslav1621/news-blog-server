import {ApiProperty} from "@nestjs/swagger";

export class PostDto{
  @ApiProperty()
  author: string;
  @ApiProperty()
  date: Date;
  @ApiProperty()
  text: string;
  @ApiProperty()
  title: string;
  @ApiProperty()
  image: string;
}