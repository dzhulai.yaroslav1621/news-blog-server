import {Body, Controller, Post} from '@nestjs/common';
import {AuthService} from "./auth.service";

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {
  }

  @Post()
  login(@Body()loginDto: {username: string, password: string}){
    return this.authService.login(loginDto)
  }
  @Post("register")
  register(@Body()loginDto: {username: string, password: string, role: string}){
    return this.authService.register(loginDto)
  }
}
