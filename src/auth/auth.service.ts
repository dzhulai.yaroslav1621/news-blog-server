import {Injectable} from '@nestjs/common';
import {DatabaseService} from "../database/database.service";

@Injectable()
export class AuthService {
  constructor(private db: DatabaseService) {
  }

  async login(loginDto: { username: string, password: string }) {
    const user = await this.db.user.findFirst({where: {userName: loginDto.username}})
    if (user.password === loginDto.password) {
      return "go"
    } else {
      return "error"
    }
  }

  async register(registerDto: { username: string, password: string, role: string }) {
    console.log(registerDto)
    await this.db.user.create({
      data: {
        password: registerDto.password,
        userName: registerDto.username,
        role: registerDto.role
      }
    })
    return await this.login({username: registerDto.username, password: registerDto.password})
  }

  // register(registerDto: any){
  //   this.db.user.findMany().then(res=>console.log(res))
  // }
}
